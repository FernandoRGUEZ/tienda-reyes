<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\InventarioController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/inventarios', [InventarioController::class, 'store']);
    Route::get('/inventarios', [InventarioController::class, 'index']);
    Route::get('/inventarios/{id}', [InventarioController::class, 'show']);
    Route::put('/inventarios/{id}', [InventarioController::class, 'update']);
    Route::delete('/inventarios/{id}', [InventarioController::class, 'destroy']);

    Route::get('/user', function (Request $request) {
        return $request->user();
    });
});
// Ruta de prueba para verificar la configuración
Route::get('/test', function () {
    return response()->json(['message' => 'API working correctly'], 200);
});
