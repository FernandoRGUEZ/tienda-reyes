<x-dialog-modal wire:model.live="modalGuardar">
    <x-slot name="title">
        {{ $title }}
    </x-slot>

    <x-slot name="content">
        <x-action-message on='success' />
        @if ($errors->any())
            <ul class="bg-red-50 w-full">
                @foreach ($errors->all() as $error)
                    <li class="text-red-500">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <form wire:submit="save" method="POST">
            @csrf
            <div class="grid grid-cols-2 gap-3">
                <div class="p-3 col-span-2 lg:col-span-1">
                    <input type="text" wire:model="form.nombre" placeholder="Nombre" class="p-1 border-2 border-gray-300 rounded-lg" />
                    @error('form.nombre') <span class="text-red-700 text-xs">{{ $message }}</span> @enderror
                </div>

                <div class="p-3 col-span-2 lg:col-span-1">
                    <input type="text" wire:model="form.cantidad" placeholder="Cantidad" class="p-1 border-2 border-gray-300 rounded-lg" />
                    @error('form.cantidad') <span class="text-red-700 text-xs">{{ $message }}</span> @enderror
                </div>

                <div class="p-3 col-span-2 lg:col-span-1">
                    <input type="number" wire:model="form.precio" placeholder="Precio" class="p-1 border-2 border-gray-300 rounded-lg" />
                    @error('form.precio') <span class="text-red-700 text-xs">{{ $message }}</span> @enderror
                </div>

                <div class="p-3 col-span-2 flex justify-center align-middle items-center">
                    <button type="submit" class="p-3 bg-blue-700 text-white hover:bg-blue-900 hover:scale-105">Guardar</button>
                </div>
            </div>
        </form>
    </x-slot>

    <x-slot name="footer">
        <x-secondary-button wire:click="$toggle('modalGuardar')" wire:loading.attr="disabled">
            Cancelar
        </x-secondary-button>
    </x-slot>
</x-dialog-modal>
