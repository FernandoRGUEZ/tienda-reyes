<div class="p-6 bg-white border-b border-gray-200">
    <div class="p-6 lg:p-8 bg-[#e3ffe1] border-b border-gray-200">
        <x-application-logo class="block h-12 w-auto" />
    </div>

    <div class="flex justify-between items-center p-2">
        <h2 class="text-lg font-medium text-gray-900">
            Inventario
        </h2>
        <button  wire:click='openModal()' class="bg-blue-500 text-white px-4 py-2 rounded-md">
            Agregar Productosd
        </button>
    </div>
    <div class="p-1 mt-3 grid grid-cols-2">
        <div class="col-span-1">
            Asignaturas:
            <input type="search" class="p-1 ml-1.5 border-2 border-blue-100 rounded-md" wire:model.live='search' placeholder="Buscar..." />
        </div>
        <div class="col-span-1 flex justify-end">
            <label for="paginacion">Registros por página:
                <select id="paginacion" wire:model.live='rows'>
                    <option value="2">2</option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </label>
        </div>
    </div>
    <table class="min-w-full divide-y divide-gray-200 mt-4">
        <thead>
            <tr>
                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    ID Producto
                </th>
                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Nombre
                </th>
                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Cantidad
                </th>
                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Precio
                </th>
                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Acciones
                </th>
            </tr>
        </thead>
        <tbody class="bg-white divide-y divide-gray-200">
            @foreach($data as $item)
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                        {{ $item->id_producto }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                        {{ $item->nombre }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                        {{ $item->cantidad }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                        ${{ $item->precio }}
                    </td>
                    <td class=" flex justify-around">
                        <button class="bg-green-200 text-white p-2 rounded-lg hover:bg-green-500" wire:click='openModal()'>
                            Modificar
                        </button>
                        <button class="bg-red-300 text-white p-2 rounded-lg hover:bg-red-600" wire:click='deleteProduct({{ $item->id_producto }})'>
                            Borrar
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <livewire:create-product2 />
</div>
