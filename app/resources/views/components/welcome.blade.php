<div class="p-6 lg:p-8 bg-[#e3ffe1] border-b border-gray-200">
    <x-application-logo class="block h-12 w-auto" />

   
</div>

<div class="bg-gray-200 bg-opacity-25 grid grid-cols-1 md:grid-cols-2 gap-6 lg:gap-8 p-6 lg:p-8">   
    <x-dashboard-table />
</div>
