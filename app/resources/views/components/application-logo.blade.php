<div class="flex gap-6 justify-center text-center">
  <img src="{{ asset('images/logo.png') }}" alt="Logo" {{ $attributes }}>

  <p class="text-3xl"><strong>La super tiendita de los reyes</strong></p>
</div>
