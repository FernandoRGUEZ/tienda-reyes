<?php

namespace App\Livewire;

use App\Livewire\Forms\FormInventario;
use App\Models\Inventario;
use Livewire\Component;

use Livewire\Attributes\On; 

use App\Livewire\Forms\InvetarioForm;

class CreateProduct2 extends Component
{
    public $modalGuardar = false;
    public $title = 'Nueva materia';

    public FormInventario $form;

    public function render()
    {
        return view('livewire.create-product2');
    }

    #[On('open-modal')] 
    public function abrirModal($id = null) {
        $materia = new Inventario();
        if($id) {
            $materia = Inventario::where('id_producto', $id)->first();
            $this->form->set($materia);
        } else {
            $this->form->clear();
        }

        $this->modalGuardar = true;
    }

    public function save() {
        if($this->form->save()) {
            $this->dispatch('guardado');
        }
        $this->modalGuardar = false;
    }
}
