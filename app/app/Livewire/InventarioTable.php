<?php

namespace App\Livewire;

use App\Models\Inventario;
use Livewire\Component;
use Livewire\Attributes\On;

class InventarioTable extends Component
{
    
    public $search;
    public $rows = 10;

    public function openModal($id = null) {
        if($id) {
            $this->dispatch('open-modal', ['id' => $id]);
        } else {
            $this->dispatch('open-modal');
        }
    }



    public function render()
    {
        $query = Inventario::orderBy('id_producto');

        if ($this->search) {
            $query = $query->where('nombre', 'ILIKE', '%' . $this->search . '%');
        }

        $data = $query->paginate($this->rows);

        return view('livewire.inventario-table', compact('data'));
    }

    #[On('guardado')]
    public function refresh() {
        
        $this->dispatch('success');
    }

    public function deleteProduct($id)
    {
        $product = Inventario::find($id);
        if ($product) {
            $product->delete();
        }
    }
}
