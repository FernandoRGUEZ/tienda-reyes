<?php

namespace App\Livewire\Forms;

use App\Models\Inventario;
use Livewire\Attributes\Validate;
use Livewire\Form;

class FormInventario extends Form
{
    
    public $id_producto;

    #[Validate('required')]
    public $nombre;

    #[Validate('required')]
    public $cantidad;

    #[Validate('required')]
    public $precio;


    function set(Inventario $materia) 
    {
        $this->id_producto = $materia->id_materia;
        $this->nombre = $materia->nombre_materia;
        $this->cantidad = $materia->clave_materia;
        $this->precio = $materia->horas_teoricas;
    }

    function clear() {
        $this->reset();
    }

    function save($id = null)
    {
        $validated = $this->validate();

        try {
            $producto = Inventario::findOrNew($this->id_producto);
            $producto->fill($validated);
            $producto->save();

            $this->reset();

            return $producto;
        } catch(\Exception $ex) {
            info($ex->getMessage());
            $this->addError('Materia', 'Error al guardar el producto');
            return null;
        }
    }
}
